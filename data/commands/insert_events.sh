#!/bin/bash


file_json=$1

if [ -z "$file_json" ]
then
     
     
     jq -rc '.[]' /opt/data/json_files/calls.json | /opt/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092  --topic call_events 
      
else
     jq -rc '.[]' /opt/data/json_files/$file_json | /opt/kafka/bin/kafka-console-producer.sh --broker-list localhost:9092  --topic call_events 
     
fi




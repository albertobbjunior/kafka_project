name := "Kafka"

version := "0.1"

scalaVersion := "2.12.0"

val sparkVersion = "3.0.0"

libraryDependencies ++= Seq(
  "org.scala-lang" % "scala-library" % scalaVersion.value,
  "org.apache.spark" %% "spark-sql" % "3.0.0-preview",
  "org.apache.spark" %% "spark-sql-kafka-0-10" % "3.0.0" % "provided",
  "org.postgresql" % "postgresql" % "42.2.14"
)

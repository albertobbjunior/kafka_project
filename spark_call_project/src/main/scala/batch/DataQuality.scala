package batch

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, DataFrame}

import scala.util.Try
class DataQuality {

  val dataPatter = new java.text.SimpleDateFormat ("yyyy-MM-dd")


  def isNullColumns(df: DataFrame): Column =
    df.columns.map(x=>col(x).isNotNull).reduce(_ && _)


  def isDoubleNumber(baseString: Option[String]): Boolean = baseString match {
    case Some(code) => !code.isEmpty && Try(code.toDouble).isSuccess
    case None => false
  }


  def isDate(baseString: Option[String]): Boolean = baseString match {
    case Some(code) => !code.isEmpty && Try(dataPatter.parse(code)).isSuccess
    case None => false
  }





}

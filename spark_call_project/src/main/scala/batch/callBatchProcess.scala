package batch

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.functions.col

class callBatchProcess {


  val key_columnsNames        = Seq("agentid","answered")
  val measures_columnsNames   = Seq("durationminutes","waitingminutes")
  val date_event_columnsNames  = Seq("answered")
  import org.apache.spark.sql.expressions.Window
  val windowSpec  = Window.partitionBy("agentid","answered_timestamp").orderBy(col("answered_timestamp").asc)



  def batch(): Unit = {

    val spark = SparkSession.builder()
      .appName("kafka-tutorials")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._

    val jdbcDF = spark.read
      .format("jdbc")
      .option("url", "jdbc:postgresql://postgres:5432/postgres")
      .option("dbtable", "raw_log_call_data")
      .option("user", "postgres")
      .option("password", "postgres")
      .load()
      .withColumn("answered_timestamp",col("answered"))

    val dataQuality = new DataQuality


    val types = Seq(
                    ("agentid", "int"),
                    ("answered", "date"),
                    ("customerscore", "int"),
                    ("durationminutes", "double"),
                    ("waitingminutes", "double"),
                    ("answered_timestamp", "timestamp")
    )



    val df_convert_type = jdbcDF.select(types.map{case (c, t) => col(c).cast(t)}: _*)

    val df = df_convert_type
                  .withColumn("empty_key_columns",
                    dataQuality.isNullColumns(
                      jdbcDF.select(key_columnsNames.head, key_columnsNames.tail: _*)
                    ))
                  .withColumn("error_measure_columns",
                    dataQuality.isNullColumns(
                      jdbcDF.select(key_columnsNames.head, key_columnsNames.tail: _*)
                    ))
                  .withColumn("invalid_date_events",
                  dataQuality.isNullColumns(
                      jdbcDF.select(date_event_columnsNames.head, date_event_columnsNames.tail: _*)
                   ))
                  .withColumn("row_number",row_number.over(windowSpec))
                  .withColumnRenamed("answered","date_answered")
                  .withColumn("agent_id",col("agentid"))



    val dfAggreg = df
                        .filter(col("empty_key_columns") && col("invalid_date_events") && col("row_number") ===1)
                        .groupBy("date_answered", "agent_id")
                        .agg(
                          min("customerscore").as("min_customerscore"),
                          max("customerscore").as("max_customerscore"),
                          avg("customerscore").as("avg_customerscore"),
                          min("durationminutes").as("min_durationminutes"),
                          max("durationminutes").as("max_durationminutes"),
                          avg("durationminutes").as("avg_durationminutes"),
                          sum("durationminutes").as("total_durationminutes"),
                          min("waitingminutes").as("min_waitingminutes"),
                          max("waitingminutes").as("max_waitingminutes"),
                          avg("waitingminutes").as("avg_waitingminutes"),
                          sum("waitingminutes").as("total_waitingminutes"),
                          count(lit(1)).alias("call_quantity")
                          )



    //persist aggregate dataframes
    persistDataFrame(dfAggreg,"call_agg_table","overwrite")


    //persist reject_dataframes
    persistDataFrame(
                    df.filter(col("empty_key_columns")===false)
                     ,"call_rejected_table",
                    "overwrite")


    //Write Last Execution ETL
    val data = Seq(("call_agg_table", "callBatchProcess"))
      .toDF("table_name","process")
      .withColumn("etl_execution",unix_timestamp().cast("timestamp"))

    persistDataFrame(data,"sensor_etl_execution","append")
  }
  //Function that move dataframe to Database
  def persistDataFrame(df: DataFrame,table:String, typeSave:String): Unit = {
    df
      .write
      .format("jdbc")
      .option("url", "jdbc:postgresql://postgres:5432/postgres")
      .option("dbtable", table)
      .option("user", "postgres")
      .option("password", "postgres")
      .mode(typeSave)
      .save()
  }





}

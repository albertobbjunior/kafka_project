package streaming

import org.apache.spark.sql.types.{DataTypes, StructType}

class PatterData() {

  def patterData(): StructType =  new  StructType()
    .add("agentId", DataTypes.StringType)
    .add("durationMinutes", DataTypes.StringType)
    .add("answered", DataTypes.StringType)
    .add("customerScore", DataTypes.StringType)
    .add("waitingMinutes", DataTypes.StringType
    )

}

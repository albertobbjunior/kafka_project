package streaming

import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.functions.from_json

import org.apache.spark.sql.types.{DataTypes, StructType}

class streamingCalls() {

  //--def streaming(args: Array[String]): Unit = {
  def streaming(): Unit = {
    val spark = SparkSession.builder()
      .appName("kafka-tutorials")
      .master("local[*]")
      .getOrCreate()

    import spark.implicits._
    val ipKafka = "kafka:9092"

    print()


    val df =
      spark.readStream
        .format("kafka")
        .option("kafka.bootstrap.servers", ipKafka)
        .option("subscribe", "call_events")
       // .option("startingOffsets", "latest")
        .option("auto.offset.reset", "earliest")

        .load()
    val typeStruct = new PatterData
    val personFlattenedDf = df.selectExpr("CAST(value AS STRING)", "topic", "offset")
                              .withColumn("call_object", (from_json($"value", typeStruct.patterData()) ))
                              .selectExpr("topic", "offset",
                                                 "call_object.durationMinutes",
                                                 "call_object.agentId",
                                                 "call_object.answered", "call_object.customerScore",
                                                 "call_object.waitingMinutes")

    val jdbcWriter = new PostgreSqlSink()
    val writeData = personFlattenedDf.writeStream
        .foreach(jdbcWriter)
        .outputMode("update")
        .start()

    writeData.awaitTermination()
  }




}
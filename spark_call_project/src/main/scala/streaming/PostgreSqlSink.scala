package streaming
import java.sql._
  class PostgreSqlSink() extends org.apache.spark.sql.ForeachWriter[org.apache.spark.sql.Row] {
    val user = "postgres"
    val pwd = "postgres"
    val url = "jdbc:postgresql://postgres:5432/postgres"
    val driver = "org.postgresql.Driver"
    var connection: java.sql.Connection = _
    var statement: java.sql.PreparedStatement = _

    val v_sql = "insert INTO raw_log_call_data  (topic,message_id,agentid,durationminutes,answered,customerscore,waitingminutes) values ( ?, ?, ?,?,?,?,?)"

    def open(partitionId: Long, version: Long): Boolean = {
      Class.forName(driver)
      connection = java.sql.DriverManager.getConnection(url, user, pwd)
      connection.setAutoCommit(true)
      statement = connection.prepareStatement(v_sql)

      true
    }
    def process(value: org.apache.spark.sql.Row): Unit = {
      // ignoring value(0) as this is address
      statement.setString(1, value(0).toString)
      statement.setString(2, value(1).toString)
      statement.setString(3, value(3).toString)
      statement.setString(4, value(2).toString)
      statement.setString(5, value(4).toString)
      statement.setString(6, value(5).toString)
      statement.setString(7, value(6).toString)
      statement.executeUpdate()

    }
    def close(errorOrNull: Throwable): Unit = {
      connection.close
    }


}

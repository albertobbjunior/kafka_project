**Data** : Folder with bash commands, drivers and JSON data to insert as producer

**Docker**: Folder with docker composer and install environment

**Help**: docker commands to Kafka and spark

**Spark_call_project**: Scala/Spark Project that moves the events to Postgres raw data and Aggregate all information

---------------------------------------------------------------------------------------------------------------------------------------------------

 ***Disclaimer: This process was created using Docker Compose and tested using Docker compose versions: 1.25.0 and 1.24.0***



 
 **STEP 1 - To create all environment, open Bash/CMD terminal into the project folder and then execute the following command**
     
      cd docker 

    ./make_docker.sh
    

 **STEP 2 -To access Postgres Database data, to execute the following commands**
 
    1 - Open a Browser page(Chrome, IE or Firefox) and acess the link http://localhost:8080
        user:postgres 
        password:postgres
        
    2 - Click on New Server and enter with informations below
        server: postgres
        user:postgres
        password:postgres

    3 - To access Events tables and aggregate tables.(Please, to confirm that there are no data into tables)

         row_events -       'SELECT from  public.raw_log_call_data;'     

         row_agg_table -    'SELECT   FROM public.call_agg_table;'     

**Step 3 - To open a New Bash/CMD command and then execute the following command**
    
    docker exec -it docker_kafka_1 bash -c '/opt/data/commands/insert_events.sh'

**Step 4 - To access again Postgres database and to confirm that there are new datas into tables**
 
        row_events -       'SELECT from  public.raw_log_call_data;'     

        row_agg_table -    'SELECT   FROM public.call_agg_table;'   

**Step 5 - To change file "data/json_files/calls_v3.json" and to execute  step 3 again to insert more data or to insert a new json file into "/data/json_files/calls_v3.json" and to execute command using the new file as parameter like example**
        
    example:    
    docker exec -it docker_kafka_1 bash -c '/opt/data/commands/insert_events.sh new_file.json'

**Step 6 - ETL that moves rows data to Aggregate table will be executed each 15 minutes but it can be executed using this following command. After that, to confirm that all information will be move to call_agg_table execute the following command**
        
    example:    
    #docker exec -ti -d spark-master bash  -c  '/deploy/data//commands/etl_agg_table.sh'


-----------------------------------------------------------------------------------------------------------------------------------------------------


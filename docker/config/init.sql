CREATE TABLE raw_log_call_data (
    topic 			varchar(40) 	NOT NULL,
	message_id		varchar(40) 	NOT NULL,
    agentId	 		varchar(40) 	NULL,
    durationMinutes varchar(40)		NULL, 
    answered 		varchar(40)		NULL, 
    customerScore 	varchar(40) 	NULL, 
    waitingMinutes	varchar(40) 	NULL
);

CREATE TABLE call_agg_table
(
    date_answered date,
    agent_id int,
    min_customerscore double precision,
    max_customerscore double precision,
    avg_customerscore double precision,
    min_durationminutes double precision,
    max_durationminutes double precision,
    avg_durationminutes double precision,
    total_durationminutes double precision,
    min_waitingminutes double precision,
    max_waitingminutes double precision,
    avg_waitingminutes double precision,
    total_waitingminutes double precision,
    call_quantity bigint NOT NULL
);


	CREATE TABLE public.sensor_etl_execution (
    table_name		varchar(40) 	NOT NULL,
	process 		varchar(40) 	NOT NULL,
    etl_execution	timestamp   	NOT NULL    
   );
#!/bin/bash
color1='\033[1;31m'  
color2='\033[0;34m'  
color3='\033[1;32m'  
export KAFKA_DATA=${PWD%/*}
echo -e "${color1}To stop and to remove all Container created by This project"
docker-compose -f $KAFKA_DATA/docker/docker-compose.yml down 

echo -e "${color2}To create and start all Container created by This project"
docker-compose -f $KAFKA_DATA/docker/docker-compose.yml up -d


echo -e "${color3}To move Jdbc to Spark Folder"
docker exec -ti -d spark-master bash  -c  'cp /deploy/data/conectores/postgresql-42.2.16.jar /spark/jars/'

echo -e "${color3}To create permission access to new commands"
docker exec -ti -d spark-master bash  -c  'chmod 777 /deploy/data/commands/*.sh'

echo -e "${color3}To executed Streaming Job Process"
docker exec -ti -d spark-master bash  -c '/spark/bin/spark-submit --jars /deploy/data/conectores/postgresql-42.2.16.jar  --packages org.apache.spark:spark-sql-kafka-0-10_2.12:3.0.0 /deploy/kafka_2.12-0.1.jar Streaming'                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      

#echo -e "${color3}To executed Batch Aggregate Process"
docker exec -ti -d spark-master bash  -c  '/deploy/data//commands/etl_agg_table.sh'


